# fface.scss

a scss mixin which makes it quick and easy to generate nice, correct, and up-to-date `@font-face` rules for webfonts

## Usage

```scss
@include fontface( $name, $src, $formats: [ woff ], $variants: [ [ null normal ] ], $local: null, $class: true )
{
    $extra;
}
```

### Params

| param | type | description |
| :----- | :---- | :----------- |
| $name | str  | name to be used in `font-family` rules<br />*ex*: `"Open Sans"` |
| $src  | str  | filename without extention and any variant naming (ie `-medium` or `-bolditalic`)<br />this assumes file naming is consistent across file types and variants<br />*ex*: `../fonts/open-sans/open-sans` |
| $formats | list | list of file extentions to use<br />*ex*: `woff ttf` |
| $variants | list | list of variants to include in format: `[variant-suffix] weight[i]`<br />`variant-suffix` is the part of the filename which defines which variant it is for<br />`weight` is the numerical weight or named weight to use<br />`i` defines whether the variant is in italics or not<br />*ex*:<br />`-Light 100i`<br />`-Bold 600`<br />`-BoldItalic 600i`<br />`-Book normal` |
| $local | str | name of local font to use if it exists on user's system<br />*ex*: `"Open Sans"` |
| $classes | bool | whether to generate classes for the font and its variants. a string can be passed instead to control class naming<br />*ex classes*: `f-open-sans`, `f-open-sans-600i`
| $extra | rules | any rules passed to the mixin will be included in each generated `@font-face` rule<br />*ex*:<br />`unicode-range: U+26;`<br />(see: <https://developer.mozilla.org/en-US/docs/Web/CSS/@font-face>) |

## Examples

```scss
@include fontface( "Open Sans", '../fonts/open-sans/open-sans', [ woff, ttf ], [ -Medium 500, -MediumItalic 500i, -Bold bold, -BoldItalic boldi, -Book normal, -BookItalic normali ] )
{
    -webkit-font-smoothing: antialiased;
}
```

```scss
@include fontface( 'My Super Font 27', '../fonts/msf27/My Super Font 27', ttf, $class: 'msf' );
```
